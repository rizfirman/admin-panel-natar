import React, {useState} from "react";
import { NavLink} from "react-router-dom";
import { Formik } from "formik";
import { TextField, Button } from "@material-ui/core";
import * as yup from "yup";
import { useHistory } from "react-router-dom";

const validationSchema = yup.object({
  email: yup.string().email().required("Email is required"),
  password: yup.string().required("Password is required"),
});

const LoginPage = (props) => {
  const {onClick} = props;
  const history = useHistory();

  const handleSubmit = (value, e) => {
    console.log(value)
    history.push("/dashboard");
    e.preventDefault();
    
  };

  return (
    <div className="login-page">
      <div className="container d-flex justify-content-end">
        <div className="card bg-transparent border-0">
          <div className="login-header">
            <h3>Welcome to Natar</h3>
            <h6>
              New here?{" "}
              <NavLink to="/signup" className="link-text">
                Create a new account
              </NavLink>
            </h6>
          </div>

          <div className="login-form">
            <Formik
              initialValues={{
                email: "",
                password: "",
              }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              {(formik) => (
                <form onSubmit={formik.handleSubmit}>
                  <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                      Email address
                    </label>
                    <TextField
                      type="email"
                      className="form-control"
                      id="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.email && Boolean(formik.errors.email)
                      }
                      helperText={formik.touched.email && formik.errors.email}
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="filled"
                    />
                  </div>
                  <div className="mb-3">
                    <div className="d-flex justify-content-between">
                      <label
                        htmlFor="exampleInputPassword1"
                        className="form-label"
                      >
                        Password
                      </label>
                      <a href="#" className="link-text">
                        Forgot your password?
                      </a>
                    </div>

                    <TextField
                      type="password"
                      className="form-control"
                      id="password"
                      value={formik.values.password}
                      
                      onChange={formik.handleChange}
                      error={formik.touched.password && Boolean(formik.errors.password)}
                      helperText={formik.touched.password && formik.errors.password}
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="filled"
                    />
                  </div>
                  <button type="submit" className="btn btn-primary" onClick={onClick} >
                    Submit
                  </button>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
