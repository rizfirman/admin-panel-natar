import React from "react";

const VerificationPage = () => {
  return (
    <div className="verification-page">
      <div className="container d-flex justify-content-end">
        <div className="card bg-transparent border-0">
        <div className="verification-header">
          <h3>
            Silakan masukan kode yang sudah dikirim ke nomor yang Anda dapatkan
          </h3>
        </div>

        <div className="verification-form">
          <form>
            <div className="mb-3">
              <input
                type="number"
                className="form-control"
                aria-describedby="otp-code"
              ></input>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
        </div>
      </div>
    </div>
  );
};

export default VerificationPage;
