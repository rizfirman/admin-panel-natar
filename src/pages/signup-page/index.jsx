import React from "react";
import { NavLink } from "react-router-dom";
import { Formik } from "formik";
import { TextField, Button } from "@material-ui/core";
import * as yup from "yup";
import { useHistory } from "react-router-dom";

const validationSchema = yup.object({
  email: yup.string().email().required("Email is required"),
  password: yup
    .string()
    .required("Password is required")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
});

const SignUpPage = () => {
  const history = useHistory();

  const handleSubmit = (value, e) => {
    console.log(value);
    history.push("/");
    e.preventDefault();
  };

  return (
    <div className="signup-page">
      <div className="container d-flex justify-content-end">
        <div className="card bg-transparent border-0">
          <div className="signup-header">
            <h3>Create an Account</h3>
            <h6>
              Have an Account?{" "}
              <NavLink to="/" className="link-text">
                Sign in
              </NavLink>
            </h6>
          </div>

          <div className="signup-form">
            <Formik
              initialValues={{
                email: "",
                password: "",
              }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              {(formik) => (
                <form onSubmit={formik.handleSubmit}>
                  <div className="mb-3">
                    <label for="exampleInputEmail1" className="form-label">
                      Email address
                    </label>
                    <TextField
                      type="email"
                      className="form-control"
                      id="email"
                      aria-describedby="emailHelp"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.email && Boolean(formik.errors.email)
                      }
                      helperText={formik.touched.email && formik.errors.email}
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="filled"
                    />
                  </div>
                  <div className="mb-3">
                    <label for="exampleInputPassword1" className="form-label">
                      Password
                    </label>
                    <TextField
                      type="password"
                      className="form-control"
                      id="password"
                      value={formik.values.password}
                      onChange={formik.handleChange}
                      error={
                        formik.touched.password &&
                        Boolean(formik.errors.password)
                      }
                      helperText={
                        formik.touched.password && formik.errors.password
                      }
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="filled"
                    />
                  </div>
                  <button type="submit" className="btn btn-primary">
                    Submit
                  </button>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SignUpPage;
