import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVideo, faBell } from "@fortawesome/free-solid-svg-icons";

const KonsultasiPage = () => {
  return (
    <div className="konsultasi-page">
      <div className="container-fluid">
        <div className="d-flex justify-content-end">
          <div className="card border-1 ">
            <div className="container">
              <h5 className="mt-5">List Konsultasi</h5>
              <div className="d-flex justify-content-center">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Konsultasi</th>
                      <th scope="col">Harga</th>
                      <th scope="col">Tanggal</th>
                      <th scope="col">Waktu</th>
                      <th scope="col">Status</th>
                      <th scope="col"></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Mark</td>
                      <td>Otto</td>
                      <td>@mdo</td>
                      <td>Mark</td>
                      <td className="pending">PENDING</td>
                      <td></td>
                      <td>
                        <div className="d-flex">
                          <div className="me-2 square text-center">
                            <FontAwesomeIcon icon={faVideo} className="icon" />
                          </div>
                          <div className="square text-center">
                            <FontAwesomeIcon icon={faBell} className="icon" />
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Jacob</td>
                      <td>Thornton</td>
                      <td>@fat</td>
                      <td>Mark</td>
                      <td className="selesai">SELESAI</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="1">Larry the Bird</td>
                      <td>@twitter</td>
                      <td>Mark</td>
                      <td>test</td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
};

export default KonsultasiPage;
