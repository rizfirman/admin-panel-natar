import React, { useState, useMemo } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { TabPanel, ChartJs } from "../../components";

const DashboardPage = () => {
  const [value, setValue] = useState(0);

  const data = [
    { name: "January", uv: 400, pv: 2400, amt: 2400 },
    { name: "February", uv: 100, pv: 2400, amt: 2400 },
    { name: "March", uv: 1, pv: 2400, amt: 2400 },
    { name: "April", uv: 400, pv: 2400, amt: 2400 },
    { name: "May", uv: 100, pv: 2400, amt: 2400 },
    { name: "June", uv: 1, pv: 2400, amt: 2400 },
    { name: "July", uv: 400, pv: 2400, amt: 2400 },
    { name: "August", uv: 100, pv: 2400, amt: 2400 },
    { name: "September", uv: 1, pv: 2400, amt: 2500 },
    { name: "October", uv: 1, pv: 2400, amt: 2400 },
    { name: "November", uv: 400, pv: 2400, amt: 2400 },
    { name: "December", uv: 100, pv: 2400, amt: 2400 },
    
  ];

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function a11yProps(index) {
    return {
      id: `nav-tab-${index}`,
      "aria-controls": `nav-tabpanel-${index}`,
    };
  }

  function LinkTab(props) {
    return (
      <Tab
        component="a"
        onClick={(event) => {
          event.preventDefault();
        }}
        {...props}
      />
    );
  }
  return (
    <div className="dashboard-page">
      <div className="container-fluid  d-flex justify-content-end">
        <div className="card ">
          <div className="card-content">
            <div className="card-header pt-5">
              <div>
                <h5 className="mx-5">Tahapan Bisnis</h5>
                <div className="d-flex justify-content-between ">
                  <p className="mt-2 mx-5">0 Task diselesaikan hari ini</p>
                  <div className="tab">
                    <Tabs
                      variant="fullWidth"
                      value={value}
                      onChange={handleChange}
                      aria-label="nav tabs example"
                      TabIndicatorProps={{
                        style: { background: "transparent" },
                      }}
                    >
                      <LinkTab label="Month" href="/drafts" {...a11yProps(0)} />
                      <LinkTab label="Week" href="/trash" {...a11yProps(1)} />
                      <LinkTab label="Day" href="/spam" {...a11yProps(2)} />
                    </Tabs>
                  </div>
                </div>
                <div
                 className="pb-5"
                >
                  <TabPanel value={value} index={0}  >
                    <ChartJs data={data} width={950} height={300} tooltip />
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    <ChartJs data={data} width={950} height={300} tooltip />
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    <ChartJs data={data} width={950} height={300} tooltip />
                  </TabPanel>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardPage;
