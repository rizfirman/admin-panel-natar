import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from "@material-ui/core/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";

const ProfilePage = () => {
    return (
        <div className="profile-page ">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 d-flex justify-content-end">
                        <div className="card border-0 mb-5 mt-5 pt-3 pb-3 bio-card">
                            <div className="position-relative rounded-circle overflow-hidden mx-auto custom-circle-image">
                                <img
                                    className="w-100 h-100"
                                    src="https://source.unsplash.com/1230x802"
                                    alt=""
                                ></img>
                            </div>
                            <div className="card-body  mt-4">
                                <h5 className=" card-title text-center">Jerry Kane</h5>
                                <p className="card-text business-name text-center">
                                    Nama Usaha
                                </p>
                                <p className="card-text text-center">
                                    Jl. Ancol Timur XIV No.1, Ancol, Kec. Regol, Kota Bandung,
                                    Jawa Barat 40254
                                </p>
                                <hr />
                                <div className="d-flex justify-content-center mb-3">
                                    <button className="btn btn-primary">Pitch Deck</button>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <button className="btn btn-primary">
                                       <i class="fa fa-whatsapp" aria-hidden="true"></i> Konsultasi sekarang
                                    </button>
                                </div>
                                <div className="mt-3 mb-3">
                                    <div className="container">
                                        <div className="d-flex justify-content-between ">
                                            <p>Task Selesai</p>

                                            <div className="number-container-1 text-center">
                                                <p>9</p>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-between ">
                                            <p>Task Belum Selesai</p>

                                            <div className="number-container text-center">
                                                <p>9</p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="container">
                                        <p className="card-text domisili">
                                            Domisili usaha Kota Cimahi
                                        </p>
                                        <p className="card-text">Provisi Jawa Barat, Indonesia</p>
                                    </div>
                                    <hr />
                                    <div className="container">
                                        <p className="card-text kuliner">Bidang usaha kuliner</p>
                                        <p className="card-text">
                                            Makanan, makanan-makanan olahan ketan
                                        </p>
                                    </div>
                                    <hr />
                                    <div className="container">
                                        <p className="card-text">
                                            Anggota Natar sejak Agustus 2021
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 pt-5">
                        <div className="row d-flex ">
                            <div className="col-md-6">
                                <div className="card stage-card-1">
                                    <div className="card-header border-0">
                                        <h6>Stage 1</h6>
                                        <div className="d-flex justify-content-between">
                                            <p>selesai</p>
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </div>

                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diajukan
                                                </label>
                                            </div>
                                        </div>
                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diverifikasi
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="card stage-card-2">
                                    <div className="card-header border-0">
                                        <h6>Stage 2</h6>
                                        <div className="d-flex justify-content-between">
                                            <p>selesai</p>
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </div>

                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diajukan
                                                </label>
                                            </div>
                                        </div>
                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diverifikasi
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row pt-5">
                            <div className="col-md-6">
                                <div className="card stage-card-3">
                                    <div className="card-header border-0">
                                        <h6>Stage 1</h6>
                                        <div className="d-flex justify-content-between">
                                            <p>selesai</p>
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </div>

                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diajukan
                                                </label>
                                            </div>
                                        </div>
                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diverifikasi
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="card stage-card-4">
                                    <div className="card-header border-0">
                                        <h6>Stage 2</h6>
                                        <div className="d-flex justify-content-between">
                                            <p>selesai</p>
                                            <FontAwesomeIcon icon={faEllipsisH} />
                                        </div>

                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diajukan
                                                </label>
                                            </div>
                                        </div>
                                        <div className="d-flex">
                                            <p className="px-3">01/03</p>
                                            <div class="form-check">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="flexRadioDefault1"
                                                ></input>
                                                <label
                                                    className="form-check-label"
                                                    for="flexRadioDefault1"
                                                >
                                                    Data diri diverifikasi
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProfilePage;
