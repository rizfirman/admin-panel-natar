import LoginPage from "./login-page";
import SignUpPage from "./signup-page";
import VerificationPage from "./verification-page";
import DashboardPage from "./dashboard-page";
import ProfilePage from "./profile-page";
import KonsultasiPage from "./konsultasi-page";
import TahapanBisnisPage from './tahapan-bisnis-page'

export {
  LoginPage,
  SignUpPage,
  VerificationPage,
  DashboardPage,
  ProfilePage,
  KonsultasiPage,
  TahapanBisnisPage
};
