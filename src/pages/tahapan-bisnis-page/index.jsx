

import MultiForm from '../../components/multiform'
  
const TahapanBisnisPage = () => {
  return (
    <div className="tahapan-bisnis">
      <div className="container-fluid">
        <div className="d-flex justify-content-end">
          <div className="card">
           <MultiForm />
          </div>
        </div>
        <div className="card"></div>
      </div>
    </div>
  );
};
export default TahapanBisnisPage;
