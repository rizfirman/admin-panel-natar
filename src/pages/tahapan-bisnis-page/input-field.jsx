import React from 'react'
import {TextField} from'@material-ui/core'
import {FieldConfig, useField} from 'formik'

 const InputField = ({label, ...props}) => {
     const[field, meta] = useField(props)
     console.log(meta)
    return (
        <TextField 
        label={label} 
        {...field} 
        {...props}
        error={meta.touched && Boolean(meta.errors)}
        helperText={meta.touched && meta.errors }
        />
    )
}
export default InputField