import React from 'react'
 const Footer = () => {
    return (
        <div className="footer">
            <div className="container-fluid d-flex justify-content-end">
                <p className="pt-2">Copyright Natar 2021</p>
            </div>
        </div>
    )
}

export default Footer