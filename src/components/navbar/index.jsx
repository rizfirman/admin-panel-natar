import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell, faSearch } from "@fortawesome/free-solid-svg-icons";
import Avatar from "@material-ui/core/Avatar";
import Popover from "@material-ui/core/Popover";
import Badge from '@material-ui/core/Badge';

const Navbar = (props) => {
  const [anchorEl, setAnchorEl] = useState(null);
const {name} = props;
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <nav className="navbar navbar-light border-bottom">
      <div className="container-fluid d-flex justify-content-end ">
        <div className="search mx-3">
          <FontAwesomeIcon icon={faSearch} />
        </div>
        <div className="notification mx-3">
          <Badge badgeContent={4} color="primary">
          <FontAwesomeIcon icon={faBell} onClick={handleClick} />
          </Badge>
         
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
          >
            <Typography>The content of the Popover.</Typography>
          </Popover>
        </div>
        <div className="greeting mx-3">
          <p>Hi, Sean</p>
        </div>
        <div className="avatar mx-3">
          <Avatar>OP</Avatar>
        </div>
      </div>
      <div className="page-name">
        <p>{name} |</p>
      </div>
    </nav>
  );
};

export default Navbar;
