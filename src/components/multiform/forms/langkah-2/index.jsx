import React from "react";
import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { Formik } from "formik";

const validationSchema = yup.object({
    problem: yup.string().required("This field is required"),
    challenge: yup.string().required("This field is required"),
    solution: yup.string().required("This field is required"),
});
const LangkahDua = ({onSubmit,formValues, setFormValues}) => {
   const {problem, challenge, solution} = formValues;
    return (
        <div className="langkah-2">
            <div className="container">
                <h6 className="pb-5 pt-5">Identifikasi Masalah (Masalah Sebelumnya)</h6>
                <Formik
                  initialValues={{
                    problem,
                    challenge,
                    solution,
                   
                  }}
                  onSubmit={onSubmit}
                  validationSchema={validationSchema}
                >
                    {(formik) => (
                        <form onSubmit={formik.handleSubmit}>
                            <div >
                                <label htmlFor="problem" className="pb-3">
                                    Problem Statement
                                </label>

                                <TextField
                                    id="problem"
                                    name="problem"
                                    type="text"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.problem}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={formik.touched.problem && Boolean(formik.errors.problem)}
                                    helperText={formik.touched.problem && formik.errors.problem}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="challenge" className="pb-3">
                                   
                                    Challenge/tantangan
                                </label>
                                <TextField
                                    id="challenge"
                                    name="challenge"
                                    type="text"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.challenge}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={formik.touched.challenge && Boolean(formik.errors.challenge)}
                                    helperText={formik.touched.challenge && formik.errors.challenge}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="solution" className="pb-3">
                                Problem Solution
                                </label>
                                <TextField
                                    id="solution"
                                    name="solution"
                                    multiline
                                    fullWidth
                                    type="text"
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.solution}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={formik.touched.solution && Boolean(formik.errors.solution)}
                                    helperText={formik.touched.solution && formik.errors.solution}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <button type="SUBMIT" className="btn btn-primary">Kirim</button>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};
export default LangkahDua;
