import LangkahSatu from "./langkah-1";
import LangkahDua from "./langkah-2";
import LangkahTiga from "./langkah-3";
import LangkahEmpat from "./langkah-4";
import LangkahLima from "./langkah-5";
import LangkahEnam from "./langkah-6";
import LangkahTujuh from "./langkah-7";
import {ReviewForm} from "./review-form";

export {
  LangkahSatu,
  LangkahDua,
  LangkahTiga,
  LangkahEmpat,
  LangkahLima,
  LangkahEnam,
  LangkahTujuh,
  ReviewForm
};
