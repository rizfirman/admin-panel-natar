import React, { useState } from "react";
import { Formik } from "formik";
import { TextField, Button } from "@material-ui/core";
import * as yup from "yup";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Checkbox } from "@material-ui/core";
import FormGroup from '@material-ui/core/FormGroup';


const FILE_SIZE = 160 * 1024;
const SUPPORTED_FORMATS = ["image/jpg", "image/jpeg", "image/gif", "image/png"];

const validationSchema = yup.object({
  name: yup.string().required("this field is required"),
  address: yup.string().required("this field is required"),
  domisili: yup.string().required("this field is required"),
  nib: yup.number().required("this field is required"),
  tanggal: yup.string().required("this field is required"),
  bidang: yup.string().required("this field is required"),
  sosmed: yup.string().required("this field is required"),
  lama: yup.string().required("this field is required"),
  omzet: yup.string().required("this field is required"),
  kondisi: yup.string().required("this field is required"),
  info: yup.string().required("this field is required"),
  pengetahuan: yup.string().required("this field is required"),
  pelatihan: yup.string().required("this field is required"),
  ktp: yup
    .mixed()
    .required("A file is required")
    .test(
      "fileSize",
      "File too large",
      (value) => value && value.size <= FILE_SIZE
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      (value) => value && SUPPORTED_FORMATS.includes(value.type)
    ),
  foto_nib: yup
    .mixed()
    .required("A file is required")
    .test(
      "fileSize",
      "File too large",
      (value) => value && value.size <= FILE_SIZE
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      (value) => value && SUPPORTED_FORMATS.includes(value.type)
    ),
  foto_produk: yup
    .mixed()
    .required("A file is required")
    .test(
      "fileSize",
      "File too large",
      (value) => value && value.size <= FILE_SIZE
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      (value) => value && SUPPORTED_FORMATS.includes(value.type)
    ),
    konfirmasi: yup.string().required("this checkbox is required"),
});
const LangkahSatu = (props) => {
  const { onSubmit } = props;
  return (
    <div className="langkah-1">
      <div className="container">
        <h5>DataPribadi</h5>
        <p>
          Data pada formulir ini diambil, dikelola, dan digunakan oleh
          Disdagkoperin Kota Cimahi untuk kebutuhan seleksi peserta pelatihan
          dan tidak akan dipindah tangankan kepada pihak manapun
        </p>
        <Formik
          initialValues={{
            name: "",
            address: "",
            domisili: "",
            nib: "",
            tanggal: "",
            bidang: "",
            sosmed: "",
            lama: "",
            omzet: "",
            kondisi: "",
            info: "",
            pengetahuan: "",
            pelatihan: "",
            ktp: "",
            foto_nib: "",
            foto_produk: "",
            konfirmasi:"",
          }}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {(formik) => (
            <form onSubmit={formik.handleSubmit}>
              <div className="name">
                <label htmlFor="name">
                  Nama Usaha <span className="text-danger mx-2">*</span>
                </label>
                <TextField
                  fullWidth
                  id="name"
                  name="name"
                  Label="Name"
                  style={{}}
                  margin="normal"
                  value={formik.values.name}
                  disabled={formik.isSubmitting}
                  onChange={formik.handleChange}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                  helperText={formik.touched.name && formik.errors.name}
                />
              </div>
              <div className="address pt-5">
                <label htmlFor="address">
                  Alamat Usaha<span className="text-danger mx-2">*</span>
                </label>
                <TextField
                  fullWidth
                  id="address"
                  name="address"
                  disabled={formik.isSubmitting}
                  style={{}}
                  margin="normal"
                  type="text"
                  value={formik.values.address}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.address && Boolean(formik.errors.address)
                  }
                  helperText={formik.touched.address && formik.errors.address}
                />
              </div>
              <div className="address pt-5">
                <label htmlFor="domisili">
                  Domisili Usaha<span className="text-danger mx-2">*</span>
                </label>

                <TextField
                  fullWidth
                  id="domisili"
                  name="domisili"
                  style={{}}
                  disabled={formik.isSubmitting}
                  margin="normal"
                  value={formik.values.domisili}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.domisili && Boolean(formik.errors.domisili)
                  }
                  helperText={formik.touched.domisili && formik.errors.domisili}
                />
              </div>
              <div className="nib pt-5">
                <label htmlFor="nib">
                  Usia(tahun)<span className="text-danger mx-2">*</span>
                </label>

                <TextField
                  fullWidth
                  id="nib"
                  name="nib"
                  style={{}}
                  margin="normal"
                  type="number"
                  disabled={formik.isSubmitting}
                  value={formik.values.nib}
                  onChange={formik.handleChange}
                  error={formik.touched.nib && Boolean(formik.errors.nib)}
                  helperText={formik.touched.nib && formik.errors.nib}
                />
              </div>
            
              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="bidang">
                    Bidanag usaha <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="bidang"
                    name="bidang"
                    value={formik.values.bidang}
                    onChange={formik.handleChange}
                    error={
                      formik.touched.bidang && Boolean(formik.errors.bidang)
                    }
                    helperText={formik.touched.bidang && formik.errors.bidang}
                  >
                    <FormControlLabel
                      value="kuliner"
                      control={<Radio />}
                      label="Kuliner (Makanan dan Minuman)"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="fesyen"
                      control={<Radio />}
                      label="Fesyen (Produsen baju, Jilbab, Mukena, Batik, Sepatu, dsb)"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="kerajinan"
                      control={<Radio />}
                      label="Industri Kerajinan (Produsen tas, keranjang, perhiasan, dsb)"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="jasa"
                      control={<Radio />}
                      label="Jasa yang menghasilkan produk (Percetakan, dsb)"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>
              <div className="sosmed pt-5">
                <label htmlFor="sosmed">
                  Nama akun sosial media usaha (jawab "tidak ada" jika belum
                  punya)<span className="text-danger mx-2">*</span>
                </label>
                <p>
                  Contoh: Facebook: Parakarsa DEsign Resources, Instagram:
                  parakarsa_dr
                </p>
                <TextField
                  fullWidth
                  id="sosmed"
                  name="sosmed"
                  style={{}}
                  margin="normal"
                  type="text"
                  disabled={formik.isSubmitting}
                  value={formik.values.sosmed}
                  onChange={formik.handleChange}
                  error={formik.touched.sosmed && Boolean(formik.errors.sosmed)}
                  helperText={formik.touched.sosmed && formik.errors.sosmed}
                />
              </div>
              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="lama">
                    Lama Usaha <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="lama"
                    name="lama"
                    value={formik.values.lama}
                    onChange={formik.handleChange}
                    disabled={formik.isSubmitting}
                    error={formik.touched.lama && Boolean(formik.errors.lama)}
                    helperText={formik.touched.lama && formik.errors.lama}
                  >
                    <FormControlLabel
                      value="baru"
                      control={<Radio />}
                      label="Baru memulai (kurang dari 1 bulan)"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="1-5 bulan"
                      control={<Radio />}
                      label="1 - 5 bulan"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="6-11 bulan"
                      control={<Radio />}
                      label="6 - 11 bulan"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="1-2 tahun"
                      control={<Radio />}
                      label="1 - 2 tahun"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>

              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="omzet">
                    Rata-rata omzet usaha per bulan (dalam 3 bulan terakhir){" "}
                    <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="omzet"
                    name="omzet"
                    value={formik.values.omzet}
                    onChange={formik.handleChange}
                    disabled={formik.isSubmitting}
                    error={formik.touched.omzet && Boolean(formik.errors.omzet)}
                    helperText={formik.touched.omzet && formik.errors.omzet}
                  >
                    <FormControlLabel
                      value="Rp0 - Rp5 juta"
                      control={<Radio />}
                      label="Rp0 - Rp5 juta"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Rp6 juta - Rp15 juta"
                      control={<Radio />}
                      label="Rp6 juta - Rp15 juta"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Rp16 juta - Rp25 juta"
                      control={<Radio />}
                      label="Rp16 juta - Rp25 juta"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Rp26 juta - Rp50 juta"
                      control={<Radio />}
                      label="Rp26 juta - Rp50 juta"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Rp51 juta - Rp80 juta"
                      control={<Radio />}
                      label="Rp51 juta - Rp80 juta"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>

              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="kondisi">
                    Kondisi usaha 3 bulan terakhir (saat Covid-19){" "}
                    <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="kondisi"
                    name="kondisi"
                    value={formik.values.kondisi}
                    onChange={formik.handleChange}
                    disabled={formik.isSubmitting}
                    error={
                      formik.touched.kondisi && Boolean(formik.errors.kondisi)
                    }
                    helperText={formik.touched.kondisi && formik.errors.kondisi}
                  >
                    <FormControlLabel
                      value="meningkat"
                      control={<Radio />}
                      label="Meningkat"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Tetap Stabil"
                      control={<Radio />}
                      label="Tetap Stabil"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Menurun Kurang dari 50%"
                      control={<Radio />}
                      label="Menurun Kurang dari 50%"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Menurun Lebih dari 50%"
                      control={<Radio />}
                      label="Menurun Lebih dari 50%"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Rp51 juta - Rp80 juta"
                      control={<Radio />}
                      label="Rp51 juta - Rp80 juta"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>

              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="info">
                    Tahu info Pendampingan Natar dari mana?{" "}
                    <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="info"
                    name="info"
                    value={formik.values.info}
                    onChange={formik.handleChange}
                    disabled={formik.isSubmitting}
                    error={formik.touched.info && Boolean(formik.errors.info)}
                    helperText={formik.touched.info && formik.errors.info}
                  >
                    <FormControlLabel
                      value="instagram"
                      control={<Radio />}
                      label="Instagram"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="whatsapp"
                      control={<Radio />}
                      label="Whatsapp"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="Disdagkoperin"
                      control={<Radio />}
                      label="Disdagkoperin"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>

              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="pengetahuan">
                    Pengetahuan dasar yang sudah Anda miliki pada Usaha saat
                    ini? <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="pengetahuan"
                    name="pengetahuan"
                    value={formik.values.pengetahuan}
                    onChange={formik.handleChange}
                    disabled={formik.isSubmitting}
                    error={
                      formik.touched.pengetahuan &&
                      Boolean(formik.errors.pengetahuan)
                    }
                    helperText={
                      formik.touched.pengetahuan && formik.errors.pengetahuan
                    }
                  >
                    <FormControlLabel
                      value="manajemen"
                      control={<Radio />}
                      label="Manajemen"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="desain"
                      control={<Radio />}
                      label="Desain"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="marketing"
                      control={<Radio />}
                      label="Marketing"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="keuangan"
                      control={<Radio />}
                      label="Keuangan"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="tidak ada di atas"
                      control={<Radio />}
                      label="Tidak Ada Di Atas"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>

              <div className="pt-5">
                <FormControl component="fieldset">
                  <label htmlFor="pelatihan">
                    Apakah Anda pernah mengikuti pelatihan seperti ini
                    sebelumnya? <span className="text-danger mx-2">*</span>
                  </label>
                  <RadioGroup
                    aria-label="pengetahuan"
                    name="pengetahuan"
                    value={formik.values.pelatihan}
                    onChange={formik.handleChange}
                    disabled={formik.isSubmitting}
                    error={
                      formik.touched.pelatihan &&
                      Boolean(formik.errors.pelatihan)
                    }
                    helperText={
                      formik.touched.pelatihan && formik.errors.pelatihan
                    }
                  >
                    <FormControlLabel
                      value="ya"
                      control={<Radio />}
                      label="Ya"
                      disabled={formik.isSubmitting}
                    />
                    <FormControlLabel
                      value="tidak"
                      control={<Radio />}
                      label="Tidak"
                      disabled={formik.isSubmitting}
                    />
                  </RadioGroup>
                </FormControl>
              </div>

              <div className="pb-5">
                <div>
                  <p>Upload scan/foto KTP <span className="text-danger mx-2" >*</span></p>
                  <input
                    type="file"
                    name="ktp"
                    className=" "
                    onChange={(event) =>
                      formik.setFieldValue("ktp", event.target.files[0])
                    }
                  ></input>
                  {formik.errors.ktp && <p>{formik.errors.ktp}</p>}
                </div>
                <div className="mb-4">
                  <p>Upload scan/foto NIB (Nomor Induk Berusaha) <span className="text-danger mx-2" >*</span></p>
                  <input
                    type="file"
                    name="foto_nib"
                    className=" "
                    onChange={(event) =>
                      formik.setFieldValue("foto_nib", event.target.files[0])
                    }
                  ></input>
                  {formik.errors.foto_nib && <p>{formik.errors.foto_nib}</p>}
                </div>
                <div className="mb-4">
                  <p>Upload foto/informasi Produk Usaha <span className="text-danger mx-2" >*</span></p>
                  <input
                    type="file"
                    name="foto_produk"
                    className=" "
                    onChange={(event) =>
                      formik.setFieldValue("foto_produk", event.target.files[0])
                    }
                  ></input>
                  {formik.errors.foto_produk && <p>{formik.errors.foto_produk}</p>}
                </div>
              </div>
                    <hr />
                    <div className="d-flex justify-content-center">
                      <FormGroup
                      aria-label="konfirmasi"
                      name="konfirmasi"
                      value={formik.values.konfirmasi}
                      onChange={formik.handleChange}
                      disabled={formik.isSubmitting}
                      error={
                        formik.touched.konfirmasi &&
                        Boolean(formik.errors.konfirmasi)
                      }
                      helperText={
                        formik.touched.konfirmasi && formik.errors.konfirmasi
                      }
                      ><FormControlLabel 
                      control={
                        <Checkbox 
                        name="konfirmasi"
                        value="konfirmasi"
                        onChange={formik.handleChange}
                        disabled={formik.isSubmitting}
                      />
                        
                      }
                      label="Apakah Anda sudah yakin dengan jawaban di atas, sebelum kami akan asistensi?"
                      />

                      </FormGroup>
                     
                    </div>
              <Button type="submit" color="primary" variant="contained">
                Submit
              </Button>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default LangkahSatu;
