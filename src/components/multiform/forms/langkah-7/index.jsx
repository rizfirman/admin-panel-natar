import React from "react";
import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { Formik } from "formik";

const validationSchema = yup.object({
    demografis: yup.string().required("This field is required"),
    geografis: yup.string().required("This field is required"),
    psikografis: yup.string().required("This field is required"),
    teknografis: yup.string().required("This field is required"),
});

const LangkahTujuh = ( {onSubmit,formValues, setFormValues}) => {

    const {demografis, geografis, psikografis, teknografis} = formValues;
    
    return (
        <div className="langkah-tujuh">
            <div className="container"> 
                <Formik
                    initialValues={{
                        demografis,
                        geografis,
                        psikografis,
                        teknografis,
                    }}
                    onSubmit={onSubmit}
                    validationSchema={validationSchema}
                >
                    {(formik) => (
                        <form onSubmit={formik.handleSubmit}>
                            <h6 className="pb-5 pt-5">Materi Keuanganan</h6>
                            <h6 className="pb-5 pt-5">Menghitung Harga Pokok Produksi</h6>
                            <div>
                                <label htmlFor="demografis" className="pb-3">
                                    Demografis
                                </label>
                                <TextField
                                    id="demografis"
                                    name="demografis"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.demografis}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.demografis && Boolean(formik.errors.demografis)
                                    }
                                    helperText={formik.touched.demografis && formik.errors.demografis}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="geografis" className="pb-3">
                                    Geografis
                                </label>
                                <TextField
                                    id="geografis"
                                   name="geografis"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.geografis}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.geografis && Boolean(formik.errors.geografis)
                                    }
                                    helperText={formik.touched.geografis && formik.errors.geografis}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="psikografis" className="pb-3">
                                    Psikografis
                                </label>
                                <TextField
                                    id="psikografis"
                                    name="psikografis"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.psikografis}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.psikografis && Boolean(formik.errors.psikografis)
                                    }
                                    helperText={formik.touched.psikografis && formik.errors.psikografis}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="teknografis" className="pb-3">
                                    Teknografis
                                </label>
                                <TextField
                                    id="teknografis"
                                   name="teknografis"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.teknografis}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.teknografis && Boolean(formik.errors.teknografis)
                                    }
                                    helperText={formik.touched.teknografis && formik.errors.teknografis}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <button type="submit"className="btn btn-primary">Kirim</button>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default LangkahTujuh;
