import React from "react";
import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { Formik } from "formik";

const validationSchema = yup.object({
  specific: yup.string().required("This field is required"),
  measureable: yup.string().required("This field is required"),
  actionable: yup.string().required("This field is required"),
  relevant: yup.string().required("This field is required"),
  time: yup.string().required("This field is required"),
});
const LangkahTiga = ({ onSubmit, formValues, setFormValues} ) => {
  const {specific, measureable, actionable, relevant, time} = formValues;
  return (
    <div>
      <div className="container">
        <h6 className="pb-5 pt-5">Motivasi Bisnis (Metode IGROW)</h6>
        <h6 className="pb-5 pt-5">Goal (1 Tujuan 1 IGROW)</h6>
        <Formik
          initialValues={{
            specific,
            measureable,
            actionable,
            relevant,
            time,
          }}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {(formik) => (
            <form onSubmit={formik.handleSubmit}>
              <div>
                <label htmlFor="specific" className="pb-3">
                  Specific
                </label>
                <TextField
                  id="specific"
                  name="specific"
                  multiline
                  fullWidth
                  rows={9}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className="pb-5"
                  value={formik.values.specific}
                  disabled={formik.isSubmitting}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.specific && Boolean(formik.errors.specific)
                  }
                  helperText={formik.touched.specific && formik.errors.specific}
                />
                <div className="d-flex justify-content-end">
                  <button className="btn btn-primary mx-2">Video</button>
                  <button className="btn btn-primary mx-2">Informasi</button>
                  <button className="btn btn-primary mx-2">Konsultasi</button>
                </div>
              </div>
              <div>
                <label htmlFor="measureable" className="pb-3">
                  Measurable
                </label>
                <TextField
                  id="measureable"
                  name="measureable"
                  multiline
                  fullWidth
                  rows={9}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className="pb-5"
                  value={formik.values.measureable}
                  disabled={formik.isSubmitting}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.measureable &&
                    Boolean(formik.errors.measureable)
                  }
                  helperText={
                    formik.touched.measureable && formik.errors.measureable
                  }
                />
                <div className="d-flex justify-content-end">
                  <button className="btn btn-primary mx-2">Video</button>
                  <button className="btn btn-primary mx-2">Informasi</button>
                  <button className="btn btn-primary mx-2">Konsultasi</button>
                </div>
              </div>
              <div>
                <label htmlFor="actionable" className="pb-3">
                  Actionable
                </label>
                <TextField
                  id="actionable"
                  name="actionable"
                  multiline
                  fullWidth
                  rows={9}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className="pb-5"
                  value={formik.values.actionable}
                  disabled={formik.isSubmitting}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.actionable &&
                    Boolean(formik.errors.actionable)
                  }
                  helperText={
                    formik.touched.acctionable && formik.errors.actionable
                  }
                />
                <div className="d-flex justify-content-end">
                  <button className="btn btn-primary mx-2">Video</button>
                  <button className="btn btn-primary mx-2">Informasi</button>
                  <button className="btn btn-primary mx-2">Konsultasi</button>
                </div>
              </div>
              <div>
                <label htmlFor="relevant" className="pb-3">
                  Relevant
                </label>
                <TextField
                  id="relevant"
                  name="relevant"
                  multiline
                  fullWidth
                  rows={9}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className="pb-5"
                  value={formik.values.relevant}
                  disabled={formik.isSubmitting}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.relevant && Boolean(formik.errors.relevant)
                  }
                  helperText={formik.touched.relevant && formik.errors.relevant}
                />
                <div className="d-flex justify-content-end">
                  <button className="btn btn-primary mx-2">Video</button>
                  <button className="btn btn-primary mx-2">Informasi</button>
                  <button className="btn btn-primary mx-2">Konsultasi</button>
                </div>
              </div>
              <div>
                <label htmlFor="time" className="pb-3">
                  Time Bound
                </label>
                <TextField
                  id="time"
                  name="time"
                  multiline
                  fullWidth
                  rows={9}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className="pb-5"
                  value={formik.values.time}
                  disabled={formik.isSubmitting}
                  onChange={formik.handleChange}
                  error={formik.touched.time && Boolean(formik.errors.time)}
                  helperText={formik.touched.time && formik.errors.time}
                />
                <div className="d-flex justify-content-end">
                  <button className="btn btn-primary mx-2">Video</button>
                  <button className="btn btn-primary mx-2">Informasi</button>
                  <button className="btn btn-primary mx-2">Konsultasi</button>
                </div>
              </div>
              <button type="submit"className="btn btn-primary">Kirim</button>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default LangkahTiga;
