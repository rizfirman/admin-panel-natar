import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';



 export const ReviewForm = ({formValues}) => { 
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
  
    function onDocumentLoadSuccess({ numPages }) {
      setNumPages(numPages);
    }

    const {problem, challenge, solution, specific, measureable, actionable, relevant, time} = formValues;
    return (
        <div className="review-form">
            <div className="container">
                <p>{problem}</p>
                <p>{challenge}</p>
                <p>{solution}</p>
                <p>{specific}</p>
                <p>{measureable}</p>
                <p>{actionable}</p>
                <p>{relevant}</p>
                <p>{time}</p>

            </div>
            <p>test</p>
        </div>
    )
}



export const Data = ({ details }) => (
    <div>
      <div className="d-flex">
        {details.map((data, index) => {
          const objKey = Object.keys(data)[0]
          const objValue = data[Object.keys(data)[0]];
          return <h3 key={index}>{`${objKey}: ${objValue}`}</h3>
        })}
      </div>
    </div>
  )
