import React from "react";
import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { Formik } from "formik";

const validationSchema = yup.object({
    demografis: yup.string().required("This field is required"),
    potensial1: yup.string().required("This field is required"),
    potensial2: yup.string().required("This field is required"),
    kampanye: yup.string().required("This field is required"),
    target: yup.string().required("This field is required"),
    tools: yup.string().required("This field is required"),
});

const LangkahEnam = (props) => {
    const { onSubmit } = props;
    return (
        <div className="langkah-6">
            <div className="container">
                <Formik
                    initialValues={{
                        demografis: "",
                        potensial1: "",
                        potensial2: "",
                        kampanye: "",
                        target: "",
                        tools: "",
                    }}
                    onSubmit={onSubmit}
                    validationSchema={validationSchema}
                >
                    {(formik) => (
                        <form onSubmit={formik.handleSubmit}>
                            <h6 className="pb-5 pt-5">Materi Marketing</h6>
                            <h6 className="pb-5 pt-5">
                                Siapa Personaliti Market Potensialmu?
                            </h6>
                            <div>
                                <label htmlFor="demografis" className="pb-3">
                                    Demografis
                                </label>
                                <TextField
                                    id="demografis"
                                    name="demografis"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.demografis}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.demografis && Boolean(formik.errors.demografis)
                                    }
                                    helperText={formik.touched.demografis && formik.errors.demografis}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <h6 className="pb-5 pt-5">
                                    Bertemu Dimana Market Potensialmu?{" "}
                                </h6>
                                <label htmlFor="potensial1" className="pb-3">
                                    Offline
                                </label>
                                <label htmlFor="potensial1" className="pb-3">
                                    Toko Online
                                </label>
                                <TextField
                                    id="potensial1"
                                    name="potensial1"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.potensial1}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.potensial1 && Boolean(formik.errors.potensial1)
                                    }
                                    helperText={formik.touched.potensial1 && formik.errors.potensial1}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <h6>Bertemu Dimana Market Potensialmu?</h6>
                                <label htmlFor="potensial2" className="pb-3">
                                    Online/Digital
                                </label>
                                <label htmlFor="potensial2" className="pb-3">
                                    Marketplace
                                </label>
                                <TextField
                                    id="potensial2"
                                    name="potensial2"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.potensial2}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.potensial2 && Boolean(formik.errors.potensial2)
                                    }
                                    helperText={formik.touched.potensial2 && formik.errors.potensial2}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <h6>Bagaimana Strategi Kampanye Marketingmu?</h6>
                                <label htmlFor="kampanye" className="pb-3">
                                    To Inform
                                </label>
                                <TextField
                                    id="kampanye"
                                    name="kampanye"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.kampanye}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.kampanye && Boolean(formik.errors.kampanye)
                                    }
                                    helperText={formik.touched.kampanye && formik.errors.kampanye}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <h6>
                                    Tentukan Target Strategimu Menggunakan Metode SMART?
                                    (Berdasarkan Strategi Kampanye)
                                </h6>
                                <label htmlFor="target" className="pb-3">
                                    Specific (Tujuan Spesifik/Detail)
                                </label>
                                <TextField
                                    id="target"
                                    name="target"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.target}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.target && Boolean(formik.errors.target)
                                    }
                                    helperText={formik.touched.target && formik.errors.target}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <h6>Marketing Tools yang Akan Digunakan?</h6>
                                <label htmlFor="tools" className="pb-3">
                                    Media sosial
                                </label>
                                <TextField
                                    id="tools"
                                    name="tools"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.tools}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.tools && Boolean(formik.errors.tools)
                                    }
                                    helperText={formik.touched.tools && formik.errors.tools}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary">Kirim</button>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default LangkahEnam;
