import React from "react";
import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { Formik } from "formik";

const validationSchema = yup.object({
    tangible: yup.string().required("This field is required"),
    fitur: yup.string().required("This field is required"),
    intagible: yup.string().required("This field is required"),
    relevant: yup.string().required("This field is required"),
    value: yup.string().required("This field is required"),
    esensi: yup.string().required("This field is required"),
});
const LangkahEmpat = (props) => {
    const { onSubmit } = props;
    return (
        <div className="langkah-4">
            <div className="container">
                <h6 className="pb-5 pt-5">Materi BIsnis (BMC)</h6>
                <h6 className="pb-3 pt-3">
                    Value Proposition/Apa keunggulan bisnis/produk saya?
                </h6>
                <Formik
                    initialValues={{
                        tangible: "",
                        fitur: "",
                        intagible: "",
                        relevant: "",
                        value: "",
                        esensi: "",
                    }}
                    onSubmit={onSubmit}
                    validationSchema={validationSchema}
                >
                    {(formik) => (
                        <form onSubmit={formik.handleSubmit}>
                            <div>
                                <label htmlFor="tangible" className="pb-3">
                                    Tangible/Terlihat
                                </label>
                                <TextField
                                    id="tangible"
                                    name="tangible"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.tangible}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.tangible && Boolean(formik.errors.tangible)
                                    }
                                    helperText={formik.touched.tangible && formik.errors.tangible}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="fitur" className="pb-3">
                                    Fitur
                                </label>
                                <TextField
                                    id="fitur"
                                    name="fitur"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.fitur}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.fitur && Boolean(formik.errors.fitur)
                                    }
                                    helperText={formik.touched.fitur && formik.errors.fitur}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="intagible" className="pb-3">
                                    Intagible/tidak terlihat
                                </label>
                                <TextField
                                    id="intagible"
                                    name="intagible"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.intagible}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.intagible && Boolean(formik.errors.intagible)
                                    }
                                    helperText={formik.touched.intagible && formik.errors.intagible}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="relevant" className="pb-3">
                                    Relevant
                                </label>
                                <TextField
                                    id="relevant"
                                    name="relevant"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.relevant}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.relevant && Boolean(formik.errors.relevant)
                                    }
                                    helperText={formik.touched.relevant && formik.errors.relevant}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="value" className="pb-3">
                                    Value
                                </label>
                                <TextField
                                    id="value"
                                    name="value"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.value}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.value&& Boolean(formik.errors.svalue)
                                    }
                                    helperText={formik.touched.value && formik.errors.value}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="esensi" className="pb-3">
                                    Esensi
                                </label>
                                <TextField
                                    id="esensi"
                                    label=""
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.esensi}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.esensi && Boolean(formik.errors.esensi)
                                    }
                                    helperText={formik.touched.esensi && formik.errors.esensi}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary">Kirim</button>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default LangkahEmpat;
