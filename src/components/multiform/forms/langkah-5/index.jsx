import React from "react";
import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { Formik } from "formik";

const LangkahLIma = (props) => {
    const { onSubmit } = props;
    const FILE_SIZE = 160 * 1024;
    const SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/gif",
        "image/png",
    ];

    const validationSchema = yup.object({
        brand: yup.string().required("This field is required"),
        karakter: yup.string().required("This field is required"),
        value: yup.string().required("This field is required"),
        filosofi: yup.string().required("This field is required"),
        kunci: yup.string().required("This field is required"),
        referensi: yup.string().required("This field is required"),
        logo: yup
            .mixed()
            .required("A file is required")
            .test(
                "fileSize",
                "File too large",
                (value) => value && value.size <= FILE_SIZE
            )
            .test(
                "fileFormat",
                "Unsupported Format",
                (value) => value && SUPPORTED_FORMATS.includes(value.type)
            ),
        kemasan: yup
            .mixed()
            .required("A file is required")
            .test(
                "fileSize",
                "File too large",
                (value) => value && value.size <= FILE_SIZE
            )
            .test(
                "fileFormat",
                "Unsupported Format",
                (value) => value && SUPPORTED_FORMATS.includes(value.type)
            ),
        tempat: yup
            .mixed()
            .required("A file is required")
            .test(
                "fileSize",
                "File too large",
                (value) => value && value.size <= FILE_SIZE
            )
            .test(
                "fileFormat",
                "Unsupported Format",
                (value) => value && SUPPORTED_FORMATS.includes(value.type)
            ),
        tools: yup
            .mixed()
            .required("A file is required")
            .test(
                "fileSize",
                "File too large",
                (value) => value && value.size <= FILE_SIZE
            )
            .test(
                "fileFormat",
                "Unsupported Format",
                (value) => value && SUPPORTED_FORMATS.includes(value.type)
            ),
    });
    return (
        <div className="langkah-5">
            <div className="container">
                <h6 className="pb-5 pt-5">MATERI DESIGN</h6>
                <h6 className="pb-5 pt-5">BRAND PERSONALITY</h6>
                <Formik
                    initialValues={{
                        brand: "",
                        karakter: "",
                        value: "",
                        filosofi: "",
                        kunci: "",
                        referensi: "",
                        logo: "",
                        kemasan: "",
                        tempat: "",
                        tools: "",
                    }}
                    onSubmit={onSubmit}
                    validationSchema={validationSchema}
                >
                    {(formik) => (
                        <form onSubmit={formik.handleSubmit}>
                            <div className="pb-5">
                                <label htmlFor="brand" className="pb-3">
                                    Ceritakan secara singkt soal usaha/brand anda? (penjelasan)
                                </label>
                                <TextField
                                    id="brand"
                                    name="brand"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.brand}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={formik.touched.brand && Boolean(formik.errors.brand)}
                                    helperText={formik.touched.brand && formik.errors.brand}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div className="pb-5">
                                <label htmlFor="karakter" className="pb-3">
                                    JIka brand/usaha anda adalah manusia, kira-kira karakter dan
                                    personanya seperti apa ya?(penjelasan)
                                </label>
                                <TextField
                                    id="karakter"
                                    name="karakter"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.karakter}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.karakter && Boolean(formik.errors.karakter)
                                    }
                                    helperText={formik.touched.karakter && formik.errors.karakter}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            
                            <div className="pb-5">
                                <label htmlFor="value" className="pb-3">
                                    Apa Value/Keunggulan yang akan kamu tawarakan dari usaha/brand
                                    anda? (penjelasan)
                                </label>
                                <TextField
                                    id="value"
                                    name="value"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.value}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.value && Boolean(formik.errors.value)
                                    }
                                    helperText={formik.touched.value && formik.errors.value}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div className="pb-5">
                                <label htmlFor="filosofi" className="pb-3">
                                    Apa filosofi dari usaha/brand anda? (penjelasan)
                                </label>
                                <TextField
                                    id="filosofi"
                                    name="filosofi"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.filosofi}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.filosofi && Boolean(formik.errors.filosofi)
                                    }
                                    helperText={formik.touched.filosofi && formik.errors.filosofi}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div className="pb-5">
                                <label htmlFor="kunci" className="pb-3">
                                    Tuliskan 4 Kata Kunci yang menggambarkan usaha/brand anda?
                                    (penjelasan)
                                </label>
                                <TextField
                                    id="kunci"
                                    name="kunci"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.kunci}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.kunci && Boolean(formik.errors.kunci)
                                    }
                                    helperText={formik.touched.kunci && formik.errors.kunci}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                           
                            <div className="pb-5">
                                <label htmlFor="referensi" className="pb-3">
                                    Siapakah referensi & kompetitor anda yang berada dalam linkup
                                    sejenis? Minimal 3, Maksimal 4(penjelasan, bisa sisipkan link)
                                </label>
                                <TextField
                                    id="referensi"
                                    name="referensi"
                                    multiline
                                    fullWidth
                                    rows={9}
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    className="pb-5"
                                    value={formik.values.referensi}
                                    disabled={formik.isSubmitting}
                                    onChange={formik.handleChange}
                                    error={
                                      formik.touched.referensi && Boolean(formik.errors.referensi)
                                    }
                                    helperText={formik.touched.referensi&& formik.errors.referensi}
                                />
                                <div className="d-flex justify-content-end">
                                    <button className="btn btn-primary mx-2">Video</button>
                                    <button className="btn btn-primary mx-2">Informasi</button>
                                    <button className="btn btn-primary mx-2">Konsultasi</button>
                                </div>
                            </div>
                            <div className="pb-5">
                                <h6 className="mb-5">Lampirkan Foto Benchmark & Kompetitor</h6>
                                <div>
                                    <p>Logo kompetitor</p>
                                    <input type="file" name="logo" className=" " onChange={(event) => formik.setFieldValue("logo", event.target.files[0])}></input>
                                    {formik.errors.logo && <p className="text-danger">{formik.errors.logo}</p>}
                                </div>
                                <div className="mb-4">
                                    <p>Kemasan kompetitor</p>
                                    <input type="file" name="kemasan" className=" " onChange={(event) => formik.setFieldValue("kemasan", event.target.files[0])}></input>
                                    {formik.errors.kemasan && <p className="text-danger">{formik.errors.kemasan}</p>}
                                </div>
                                <div className="mb-4">
                                    <p>Tempat/Interiror/exterior</p>
                                    <input type="file"name="tempat"className=" " onChange={(event) => formik.setFieldValue("tempat", event.target.files[0])}></input>
                                    {formik.errors.tempat && <p className="text-danger">{formik.errors.tempat}</p>}
                                </div>
                                <div className="mb-4">
                                    <p>
                                        Salah satu Marketing tools kompetitor(Menu, Brosur, Konten
                                        dll)
                                    </p>
                                    <input type="file"  name="tools" className=" " onChange={(event) => formik.setFieldValue("tools", event.target.files[0])}></input>
                                    {formik.errors.tools && <p>{formik.errors.tools}</p>}
                                </div>

                                <button type="submit" className="btn btn-primary">Kirim</button>
                            </div>
                        </form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default LangkahLIma;
