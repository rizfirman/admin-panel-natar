import React from "react";
import {
  LangkahSatu,
  LangkahDua,
  LangkahTiga,
  LangkahEmpat,
  LangkahLima,
  LangkahEnam,
  LangkahTujuh,
  ReviewForm
} from "./forms";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Typography from "@material-ui/core/Typography";
import { TextField, Button } from "@material-ui/core";
import { Formik } from "formik";
import * as yup from "yup";
import { useForm} from 'react-hooks-helper'

const defaultValue = {
  demografis: "",
  geografis: "",
  psikografis: "",
  teknografis: "",
  problem:"",
  challenge:"",
  solution:"",
  specific: "",
  measureable: "",
  actionable: "",
  relevant: "",
  time: "",
}

function getSteps() {
  return [
    "Langkah 1 (Profile Anda)",
    "Langkah 2",
    "Langkah 3",
    "Langkah 4",
    "Langkah 5",
    "Langkah 6",
    "Langkah 7",
    "Review",
  ];
}

const MultiForm = () => {
  const [activeStep, setActiveStep] = React.useState(0);
  const [formValues, setFormValues] = React.useState(defaultValue);


  const props = {formValues, setFormValues}

  const steps = getSteps();
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };
  const handleReset = () => {
    setActiveStep(0);
  };
  function getStepContent(stepIndex) {
      switch (stepIndex) {
          case 0:
              return <LangkahSatu onSubmit={handleSubmit}  {...props} />;
          case 1:
              return <LangkahDua onSubmit={handleSubmit}  {...props}/>;
          case 2:
              return <LangkahTiga onSubmit={handleSubmit}  {...props}/>;
          case 3:
              return <LangkahEmpat onSubmit={handleSubmit}  {...props}/>;
          case 4:
              return <LangkahLima onSubmit={handleSubmit}  {...props}/>;
          case 5:
              return <LangkahEnam onSubmit={handleSubmit}  {...props}/>;
          case 6:
              return <LangkahTujuh onSubmit={handleSubmit}  {...props}/>;
              case 7:
                return <ReviewForm {...props}/>;
          default:
              return "Unknown stepIndex";
      }
  }
  // const getStepContent = (step) => {
  //   const allSteps = [
  //     <LangkahSatu onSubmit={handleSubmit} />,
  //     <LangkahDua onSubmit={handleSubmit} {...props} />,
  //     <LangkahTiga onSubmit={handleSubmit} {...props}/>,
  //     <LangkahEmpat onSubmit={handleSubmit} />,
  //     <LangkahLima onSubmit={handleSubmit} />,
  //     <LangkahEnam onSubmit={handleSubmit} />,
  //     <LangkahTujuh onSubmit={handleSubmit} {...props} />,
  //   ];

  //   return (
  //     <>
  //       {allSteps.map((stepCmp, index) => {
  //         return (
  //           <div key={index} hidden={index !== step}>
  //             {stepCmp}
  //           </div>
  //         );
  //       })}
  //     </>
  //   );
  // };
  const handleSubmit = (value, e) => {
    console.log(value);
    alert(JSON.stringify(value, null, 2));
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setFormValues(value);
  };

  return (
    <div className="multi-form">
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div className="card">
              
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            {getStepContent(activeStep)}

            <div className="d-flex justify-content-end mt-5 mx-5 mb-5">
              <Button disabled={activeStep === 0} onClick={handleBack}>
                Back
              </Button>
              <Button variant="contained"  color="primary" onClick={handleNext}>
                {activeStep === steps.length - 1 ? "Finish" : "Next"}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default MultiForm;
