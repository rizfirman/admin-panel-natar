import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faDoorOpen, faUserAlt, faChartBar, faCommentDots, faChartLine} from "@fortawesome/free-solid-svg-icons"
const SideBarLogin = () => {
  return (
    <div className="sidebar-login" id="sidebar-login">
      <div className="sidebar-login-content ">
        <div className="sidebar-logo d-flex justify-content-center">
          <div>LOGO</div>
        </div>
        <div className="d-flex justify-content-center">
          <hr />
        </div>

        <div className="sidebar-login-nav">
          <NavLink to="/Dashboard" className="nav-link">
          <FontAwesomeIcon icon={faChartLine} className="me-2"/>
          Dashboard
          </NavLink>
          <NavLink to="/tahapan-bisnis" className="nav-link"><FontAwesomeIcon icon={faChartBar} className="me-2"/>Tahapan Bisnis</NavLink>
          <NavLink to="/profile" className="nav-link"><FontAwesomeIcon icon={faUserAlt} className="me-2"/>Profile</NavLink>
          <NavLink to="/konsultasi" className="nav-link"><FontAwesomeIcon icon={faCommentDots} className="me-2"/>Konsultasi</NavLink>
          <NavLink to="/" className="nav-link" exact><FontAwesomeIcon icon={faDoorOpen} className="me-2"/>Keluar</NavLink>
        </div>
      </div>
    </div>
  );
};

export default SideBarLogin;
