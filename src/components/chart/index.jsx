import React from "react";
import { LineChart, Line, CartesianGrid, XAxis, YAxis } from "recharts";
const ChartJs = (props) => {
    const { data, width, height } = props;
    return (
        <div className="chart">
            <LineChart width={width} height={height} data={data}>
                <Line type="monotone" dataKey="uv" stroke="#8884d8" />
                <CartesianGrid stroke="#ccc" />
                <XAxis dataKey="name" />
                <YAxis />
            </LineChart>
        </div>
    );
};

export default ChartJs;
