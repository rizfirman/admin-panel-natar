import { slide as Menu } from "react-burger-menu";
import Sidebar from "react-sidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes as Close } from "@fortawesome/free-solid-svg-icons";
import logo from './logo.png'

const SideBar = () => {
  return (
    <div className="sidebar"id="sidebar">
      <div className="sidebar-content">
        <div className="sidebar-logo d-flex justify-content-center">
          <img src={logo} alt="logo" ></img>
        </div>
        <div className="sidebar-tagline d-flex justify-content-center">
          <h5 className="tagline">Terus Berdampingan</h5>
        </div>
      </div>
     
    </div>

  );
};

export default SideBar;
