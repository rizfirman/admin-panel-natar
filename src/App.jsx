import React, { useState } from "react";
import {
  LoginPage,
  SignUpPage,
  VerificationPage,
  DashboardPage,
  ProfilePage,
  KonsultasiPage,
  TahapanBisnisPage,
} from "./pages";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useLocation,
} from "react-router-dom";
import { SideBar, NavBar, Footer, SideBarLogin } from "./components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes as Close } from "@fortawesome/free-solid-svg-icons";

function App(props) {
  let switchSideBar =
    window.location.pathname === "/" && "/signup" && "/verification" ? (
      <SideBar />
    ) : (
      <SideBarLogin />
    );
  return (
    <div className="App" id="App">
      <Router forceRefresh={true}>
        <Route
          render={() => {
            if (window.location.pathname === "/") {
              return <SideBar />;
            }
            if (window.location.pathname === "/signup") {
              return <SideBar />;
            }
            if (window.location.pathname === "/verification") {
              return <SideBar />;
            } else {
              return <SideBarLogin />;
            }
          }}
        />
        <Switch>
          <Route path="/" component={LoginPage} exact />
          <Route path="/signup" component={SignUpPage} />
          <Route path="/verification" component={VerificationPage} />

          <>
            <Route path="/dashboard">
              <NavBar name="Dashboard" />
              <DashboardPage />
            </Route>
            <Route path="/profile">
              <NavBar name="Profile" />
              <ProfilePage />
            </Route>
            <Route path="/konsultasi">
              <NavBar name="Konsultasi" />
              <KonsultasiPage />
            </Route>
            <Route path="/tahapan-bisnis">
              <NavBar name="Tahapan Bisnis" />
              <TahapanBisnisPage />
            </Route>
            <Footer />
          </>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
